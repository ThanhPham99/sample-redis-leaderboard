import {
  Controller,
  Post
} from '@overnightjs/core'
import {
  NextFunction,
  Request,
  Response
} from 'express'
import BaseResponse from '../utils/BaseResponse'
import ErrorHandler from '../utils/ErrorHandler'
import { LeaderboardManager } from './../services/redis/LeaderboardManager'

@Controller('api/leaderboard')
export class LeaderboardController {
  private leaderboardManager = new LeaderboardManager()

  @Post('get')
  public async get(req: Request, res: Response, next: NextFunction) {
    const task = async () => {
      const data = await this.leaderboardManager.getLeaderboard(10)
      return new BaseResponse({
        data
      })
    }
    await ErrorHandler.APIReqHandler(task, { req, res, next })
  }

  @Post('update')
  public async update(req: Request, res: Response, next: NextFunction) {
    const task = async () => {
      const { id, point, name } = req.body
      await this.leaderboardManager.updateUserPoint(id, point, name)
      return new BaseResponse({
        message: 'OK'
      })
    }
    await ErrorHandler.APIReqHandler(task, { req, res, next })
  }

  @Post('user-ranking')
  public async userRanking(req: Request, res: Response, next: NextFunction) {
    const task = async () => {
      const { id } = req.body
      const data = await this.leaderboardManager.getUserRanking(id)
      return new BaseResponse({
        data
      })
    }
    await ErrorHandler.APIReqHandler(task, { req, res, next })
  }
}

import { redis } from './redis'

export class LeaderboardManager {
  private readonly PREFIX = 'demo:'
  private readonly REDIS_KEY = this.PREFIX + 'leaderboard'
  private readonly REDIS_DATA_KEY = this.PREFIX + 'leaderboard_data'

  async getLeaderboard(limit: number) {
    try {
      const result = []
      const userRankingSet = await redis.zrevrange(this.REDIS_KEY, 0, limit, 'WITHSCORES')
      const topUserScore = userRankingSet.filter((value, index) => {
        if (index % 2 === 1) return value
        return false
      })
      const topUserId = userRankingSet.filter((value, index) => {
        if (index % 2 === 0) return value
        return false
      })
      const listUsername = await redis.hmget(this.REDIS_DATA_KEY, ...topUserId)
      for (let i = 0; i < topUserScore.length; i++) {
        result.push({
          id: topUserId[i],
          ranking: i + 1,
          username: listUsername[i],
          point: topUserScore[i]
        })
      }
      return result
    } catch (error) {
      return []
    }
  }

  async getUserRanking(userId: number) {
    const rankingInRedis = await redis.zrevrank(this.REDIS_KEY, `${userId}`)
    return rankingInRedis + 1
  }

  async updateUserPoint(userId: number, point: number, username: string) {
    await redis.zadd(this.REDIS_KEY, point, userId)
    await redis.hsetnx(this.REDIS_DATA_KEY, `${userId}`, username)
  }
}

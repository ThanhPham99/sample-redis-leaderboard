class BaseResponse {
  /**
   * Trạng thái response
   */
  errorCode: number
  /**
   * Dữ liệu trả về
   */
  data: any
  /**
   * Message trả về
   */
  message: string
  /**
   * Thời điểm khởi tạo response
   */
  timestamp: number

  constructor(input: {
    errorCode?: number,
    data?: any,
    message?: string
  }) {
    this.errorCode = input.errorCode || 0
    this.data = input.data || null
    this.message = input.message || ''
    this.timestamp = new Date().getTime()
  }
}

export default BaseResponse
